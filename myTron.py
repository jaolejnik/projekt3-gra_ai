from Player import *
from Path import *
from random import *

def show_grid():
    x = GRID_SIZE
    while x < WIDTH:
        pygame.draw.line(display_window, WHITE, (x, 0), (x, HEIGHT), 1)
        x += GRID_SIZE

    y = GRID_SIZE
    while y < HEIGHT:
        pygame.draw.line(display_window, WHITE, (0, y), (WIDTH, y), 1)
        y += GRID_SIZE


def text_objects(text, font, color):
    text_surface = font.render(text, True, color)
    return text_surface, text_surface.get_rect()


def display_message(type, size, message, color, pos):
    text = pygame.font.Font(type , size)
    text_surf, text_rect = text_objects(message, text, color)
    text_rect.center = pos
    display_window.blit(text_surf, text_rect)


def game_intro():
    intro = True

    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    intro = False

        display_window.fill((0, 0, 0))
        display_message('freesansbold.ttf', 50, 'SIMPLE TRON', WHITE, (WIDTH / 2, HEIGHT / 4))
        display_message('freesansbold.ttf', 30, 'CONTROLS', WHITE, (WIDTH / 2, HEIGHT / 2))
        display_message('freesansbold.ttf', 25, 'PLAYER 1', BLUE, (WIDTH / 4, HEIGHT / 1.75))
        display_message('freesansbold.ttf', 25, 'A & D', BLUE, (WIDTH / 4, HEIGHT / 1.50))
        display_message('freesansbold.ttf', 25, 'PLAYER 2', ORANGE, (3 * WIDTH / 4, HEIGHT / 1.75))
        display_message('freesansbold.ttf', 25, 'J & L', ORANGE, (3 * WIDTH / 4, HEIGHT / 1.50))
        pygame.display.update()


def display_winner(name, color):
    show = True

    while show:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    show = False

        display_message('freesansbold.ttf', 70, name + ' WINS', WHITE, (WIDTH / 2 + 1, HEIGHT / 2))
        display_message('freesansbold.ttf', 70, name + ' WINS', WHITE, (WIDTH / 2 - 1, HEIGHT / 2))
        display_message('freesansbold.ttf', 70, name + ' WINS', WHITE, (WIDTH / 2, HEIGHT / 2 + 1))
        display_message('freesansbold.ttf', 70, name + ' WINS', WHITE, (WIDTH / 2, HEIGHT / 2 - 1))
        display_message('freesansbold.ttf', 70, name+' WINS', color, (WIDTH / 2, HEIGHT / 2))
        pygame.display.update()


def game_loop():
    rand_angle1 = choice([0, 90, 180, 270])
    rand_angle2 = choice([0, 90, 180, 270])
    rand_position1 = (randrange(0 + 80, WIDTH - 80, 10), randrange(0 + 80, HEIGHT - 80, 10))
    rand_position2 = (randrange(0 + 80, WIDTH - 80, 10), randrange(0 + 80, HEIGHT - 80, 10))

    p1 = Player(rand_position1, rand_angle1, BLUE, keys1)
    path1 = Path((p1.pos_x, p1.pos_y), p1.color)
    p2 = Player(rand_position2, rand_angle2, ORANGE, keys2)
    path2 = Path((p2.pos_x, p2.pos_y), p2.color)

    while not p1.crashed and not p2.crashed:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                p1.turn(event.key)
                p2.turn(event.key)

        path1.update((p1.pos_x, p1.pos_y))
        path2.update((p2.pos_x, p2.pos_y))
        p1.move([path1.storage, path2.storage])
        p2.move([path1.storage, path2.storage])
        display_window.fill((0, 0, 0))
        # show_grid()
        path1.draw()
        path2.draw()
        p1.draw()
        p2.draw()

        pygame.display.update()
        clock.tick(20)

    if p1.crashed:
        display_winner('PLAYER 2', ORANGE)
    elif p2.crashed:
        display_winner('PLAYER 1', BLUE)


pygame.init()

game_intro()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

    game_loop()
