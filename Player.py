from math import *
from setup import *


class Player:
    def __init__(self, pos, angle, color, keys):
        self.pos_x = pos[0]
        self.pos_y = pos[1]
        self.color = color
        self.angle = radians(angle)
        self.keys = keys
        self.speed = 10
        self.size = GRID_SIZE
        self.rect = pygame.Rect(self.pos_x, self.pos_y, self.size, self.size)
        self.crashed = False

    def draw(self):
        self.rect = pygame.Rect(self.pos_x, self.pos_y, self.size, self.size)
        pygame.draw.rect(display_window, self.color, self.rect, 0)

    def rotate(self, angle):
        self.angle += radians(angle)

    def wall_collision(self):
        if self.pos_x <= 0 or self.pos_x + self.size >= WIDTH or\
                self.pos_y <= 0 or self.pos_y + self.size >= HEIGHT:
            self.crashed = True

    def path_collision(self, paths):
        for path in paths:
            for point in path:
                if self.angle % radians(360) == radians(0):
                    if point[0] == self.pos_x + self.size and point[1] == self.pos_y:
                        self.crashed = True
                elif self.angle % radians(360) == radians(90):
                    if point[0] == self.pos_x and point[1] == self.pos_y - self.size:
                        self.crashed = True
                elif self.angle % radians(360) == radians(180):
                    if point[0] == self.pos_x - self.size and point[1] == self.pos_y:
                        self.crashed = True
                elif self.angle % radians(360) == radians(270):
                    if point[0] == self.pos_x and point[1] == self.pos_y + self.size:
                        self.crashed = True

    def move(self, paths):
        self.wall_collision()
        self.path_collision(paths)
        if not self.crashed:
            move_vector = (self.speed * cos(self.angle), self.speed * sin(self.angle))
            self.pos_x += move_vector[0]
            self.pos_y -= move_vector[1]

    def turn(self, key_pressed):
        if key_pressed == self.keys[1]:
            self.rotate(-90)
        if key_pressed == self.keys[0]:
            self.rotate(90)