from setup import *


class Path:
    def __init__(self, player_pos, color):
        self.storage = [player_pos]
        self.color = (color[0], color[1] + 50, color[2])
        self.size = GRID_SIZE

    def update(self, player_pos):
        self.storage.append(player_pos)

    def draw(self):
        for i in self.storage:
            square = pygame.Rect(i[0], i[1], self.size, self.size)
            pygame.draw.rect(display_window, self.color, square, 0)
