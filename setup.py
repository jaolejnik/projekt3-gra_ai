import pygame

# Display constants
WIDTH = 640
HEIGHT = 480
GRID_SIZE = 10

# Colors
BLUE = (0, 128, 255)
ORANGE = (255, 128, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Displayed window
display_window = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('myTron')
clock = pygame.time.Clock()

# Key schemes
keys1 = (pygame.K_j, pygame.K_l)
keys2 = (pygame.K_a, pygame.K_d)




